export const hueToColor = (hue: number, saturation = 100, light = 50): string =>
  `hsl(${hue}, ${saturation}%, ${light}%)`;

export const hueToX = (hue: number, offset = 0): number =>
  Math.sin(((180 - hue + offset) * Math.PI) / 180);

export const hueToY = (hue: number, offset = 0): number =>
  Math.cos(((180 - hue + offset) * Math.PI) / 180);

export enum HueMark {
  Good = "good",
  Okay = "okay",
  Bad = "bad",
}

export const hueDiff = (hue1: number, hue2: number): number => {
  const lost = hue1 - hue2;
  const absLost = Math.abs(lost);
  return absLost < 180 ? lost : (360 - absLost) * (lost < 0 ? 1 : -1);
};

export const lostHueToMark = (lostHue: number): HueMark => {
  lostHue = Math.abs(lostHue);
  if (lostHue <= 45) return HueMark.Good;
  else if (lostHue <= 90) return HueMark.Okay;
  else return HueMark.Bad;
};

export const lostHueToScore = (
  lostHue: number,
  format = false
): number | string => {
  const score = 90 - Math.abs(lostHue);
  return format ? `${score > 0 ? "+" : ""}${score}` : score;
};

export enum Direction {
  Left = "left",
  Down = "down",
  Up = "up",
  Right = "right",
}

export const directionX = (dir: Direction): -1 | 1 | 0 =>
  dir === Direction.Left ? -1 : dir === Direction.Right ? 1 : 0;

export const directionY = (dir: Direction): -1 | 1 | 0 =>
  dir === Direction.Up ? -1 : dir === Direction.Down ? 1 : 0;
