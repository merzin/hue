import { hueToColor, Direction } from "./utils";

let sequence = 0;

export default class Tile {
  public id: number;
  public direction?: Direction;
  public hue: number;

  public get color(): string {
    return hueToColor(this.hue);
  }

  public constructor(hue: number) {
    this.id = sequence++;
    this.hue = hue % 360;
  }
}
