import { hueDiff, hueToX, hueToY } from "./utils";

test("hueToX", () => {
  expect(hueToX(-270)).toBe(1);
  expect(hueToX(-180)).toBeCloseTo(0, 4);
  expect(hueToX(-90)).toBe(-1);
  expect(hueToX(0)).toBeCloseTo(0, 4);
  expect(hueToX(90)).toBe(1);
  expect(hueToX(180)).toBeCloseTo(0, 4);
  expect(hueToX(270)).toBe(-1);
  expect(hueToX(360)).toBeCloseTo(0, 4);
});

test("hueToY", () => {
  expect(hueToY(-270)).toBeCloseTo(0, 4);
  expect(hueToY(-180)).toBe(1);
  expect(hueToY(-90)).toBeCloseTo(0, 4);
  expect(hueToY(0)).toBe(-1);
  expect(hueToY(90)).toBeCloseTo(0, 4);
  expect(hueToY(180)).toBe(1);
  expect(hueToY(270)).toBeCloseTo(0, 4);
  expect(hueToY(360)).toBe(-1);
});

for (let i = 0; i < 360; i += 60) {
  const complA = (i + 120) % 360;
  const complB = (i + 240) % 360;
  test(`hueDiff ${i} : (${complA} ~ -120, ${complB} ~ +120)`, () => {
    expect(hueDiff(i, complA)).toBe(-120);
    expect(hueDiff(i, complB)).toBe(120);
  });
}
