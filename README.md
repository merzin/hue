<div align="center">
  <h1>
    <img src="./logo.svg" height=64 />
    <br />
    Hue
  </h1>
  <div>
    <a href="https://gitlab.com/merzin/hue/-/commits/master">
      <img alt="pipeline status" src="https://gitlab.com/merzin/hue/badges/master/pipeline.svg" />
    </a>
    <a href="https://gitlab.com/merzin/hue/-/commits/master">
      <img alt="coverage report" src="https://gitlab.com/merzin/hue/badges/master/coverage.svg" />
    </a>
  </div>
  <p>
    Hue guessing game available at
    <a href="https://hue.merzin.dev">hue.merzin.dev</a>
    or
    <a href="https://merzin.gitlab.io/hue">merzin.gitlab.io/hue</a>.
  </p>
</div>

## Setup

Clone repositoy `git clone https://gitlab.com/merzin/hue`

Change directory `cd hue`

Install dependencies `yarn install` or `npm install`

Start development server `yarn dev` or `npm run dev`
